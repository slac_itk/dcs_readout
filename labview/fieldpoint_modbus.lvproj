﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="15008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Dependencies" Type="Dependencies"/>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
	<Item Name="FP Target" Type="RT FIELDPOINT">
		<Property Name="_fpprop_tempname" Type="Str">FP Target</Property>
		<Property Name="alias.name" Type="Str">FP Target</Property>
		<Property Name="alias.value" Type="Str">192.168.4.131</Property>
		<Property Name="Bound" Type="Bool">false</Property>
		<Property Name="CCSymbols" Type="Str">RT,1;FP,1;TARGET_TYPE,RT;OS,PharLap;CPU,x86;</Property>
		<Property Name="host.ResponsivenessCheckEnabled" Type="Bool">true</Property>
		<Property Name="host.ResponsivenessCheckPingDelay" Type="UInt">5000</Property>
		<Property Name="host.ResponsivenessCheckPingTimeout" Type="UInt">1000</Property>
		<Property Name="host.TargetCPUID" Type="UInt">3</Property>
		<Property Name="host.TargetOSID" Type="UInt">15</Property>
		<Property Name="target.cleanupVisa" Type="Bool">false</Property>
		<Property Name="target.FPProtocolGlobals_ControlTimeLimit" Type="Int">300</Property>
		<Property Name="target.getDefault-&gt;WebServer.Port" Type="Int">80</Property>
		<Property Name="target.getDefault-&gt;WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.IOScan.Faults" Type="Str"></Property>
		<Property Name="target.IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="target.IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="target.IOScan.Period" Type="UInt">10000</Property>
		<Property Name="target.IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="target.IOScan.Priority" Type="UInt">0</Property>
		<Property Name="target.IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="target.IsRemotePanelSupported" Type="Bool">true</Property>
		<Property Name="target.RTCPULoadMonitoringEnabled" Type="Bool">true</Property>
		<Property Name="target.RTDebugWebServerHTTPPort" Type="Int">8001</Property>
		<Property Name="target.RTTarget.ApplicationPath" Type="Path">/c/ni-rt/startup/startup.rtexe</Property>
		<Property Name="target.RTTarget.EnableFileSharing" Type="Bool">true</Property>
		<Property Name="target.RTTarget.IPAccess" Type="Str">+*</Property>
		<Property Name="target.RTTarget.LaunchAppAtBoot" Type="Bool">true</Property>
		<Property Name="target.RTTarget.VIPath" Type="Path">/c/ni-rt/startup</Property>
		<Property Name="target.server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.tcp.access" Type="Str">+*</Property>
		<Property Name="target.server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="target.server.tcp.paranoid" Type="Bool">true</Property>
		<Property Name="target.server.tcp.port" Type="Int">3363</Property>
		<Property Name="target.server.tcp.serviceName" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.tcp.serviceName.default" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.vi.access" Type="Str">+*</Property>
		<Property Name="target.server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="target.server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.WebServer.Config" Type="Str">Listen 8000

NI.ServerName default
DocumentRoot "$LVSERVER_DOCROOT"
TypesConfig "$LVSERVER_CONFIGROOT/mime.types"
DirectoryIndex index.htm
WorkerLimit 10
InactivityTimeout 60

LoadModulePath "$LVSERVER_MODULEPATHS"
LoadModule LVAuth lvauthmodule
LoadModule LVRFP lvrfpmodule

#
# Pipeline Definition
#

SetConnector netConnector

AddHandler LVAuth
AddHandler LVRFP

AddHandler fileHandler ""

AddOutputFilter chunkFilter


</Property>
		<Property Name="target.WebServer.Enabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogEnabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogPath" Type="Path">/c/ni-rt/system/www/www.log</Property>
		<Property Name="target.WebServer.Port" Type="Int">80</Property>
		<Property Name="target.WebServer.RootPath" Type="Path">/c/ni-rt/system/www</Property>
		<Property Name="target.WebServer.TcpAccess" Type="Str">c+*</Property>
		<Property Name="target.WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.WebServer.ViAccess" Type="Str">+*</Property>
		<Property Name="target.webservices.SecurityAPIKey" Type="Str">PqVr/ifkAQh+lVrdPIykXlFvg12GhhQFR8H9cUhphgg=:pTe9HRlQuMfJxAG6QCGq7UvoUpJzAzWGKy5SbZ+roSU=</Property>
		<Property Name="target.webservices.ValidTimestampWindow" Type="Int">15</Property>
		<Property Name="Type" Type="Str">cFP-2120</Property>
		<Item Name="cFP-2120 @0" Type="FP Device">
			<Item Name="DIP Switch 3" Type="FP Point"/>
			<Item Name="DIP Switch 4" Type="FP Point"/>
			<Item Name="Input 1" Type="FP Point"/>
			<Item Name="Input 2" Type="FP Point"/>
			<Item Name="LED A" Type="FP Point"/>
			<Item Name="LED B" Type="FP Point"/>
			<Item Name="LED C" Type="FP Point"/>
			<Item Name="LED D" Type="FP Point"/>
			<Item Name="Power Source" Type="FP Point"/>
		</Item>
		<Item Name="cFP-AI-111 @8" Type="FP Device">
			<Item Name="All" Type="FP Point"/>
			<Item Name="Channel 0" Type="FP Point"/>
			<Item Name="Channel 1" Type="FP Point"/>
			<Item Name="Channel 2" Type="FP Point"/>
			<Item Name="Channel 3" Type="FP Point"/>
			<Item Name="Channel 4" Type="FP Point"/>
			<Item Name="Channel 5" Type="FP Point"/>
			<Item Name="Channel 6" Type="FP Point"/>
			<Item Name="Channel 7" Type="FP Point"/>
			<Item Name="Channel 8" Type="FP Point"/>
			<Item Name="Channel 9" Type="FP Point"/>
			<Item Name="Channel 10" Type="FP Point"/>
			<Item Name="Channel 11" Type="FP Point"/>
			<Item Name="Channel 12" Type="FP Point"/>
			<Item Name="Channel 13" Type="FP Point"/>
			<Item Name="Channel 14" Type="FP Point"/>
			<Item Name="Channel 15" Type="FP Point"/>
		</Item>
		<Item Name="cFP-RTD-122 @1" Type="FP Device">
			<Item Name="All" Type="FP Point"/>
			<Item Name="Channel 0" Type="FP Point"/>
			<Item Name="Channel 1" Type="FP Point"/>
			<Item Name="Channel 2" Type="FP Point"/>
			<Item Name="Channel 3" Type="FP Point"/>
			<Item Name="Channel 4" Type="FP Point"/>
			<Item Name="Channel 5" Type="FP Point"/>
			<Item Name="Channel 6" Type="FP Point"/>
			<Item Name="Channel 7" Type="FP Point"/>
		</Item>
		<Item Name="cFP-RTD-122 @2" Type="FP Device">
			<Item Name="All" Type="FP Point"/>
			<Item Name="Channel 0" Type="FP Point"/>
			<Item Name="Channel 1" Type="FP Point"/>
			<Item Name="Channel 2" Type="FP Point"/>
			<Item Name="Channel 3" Type="FP Point"/>
			<Item Name="Channel 4" Type="FP Point"/>
			<Item Name="Channel 5" Type="FP Point"/>
			<Item Name="Channel 6" Type="FP Point"/>
			<Item Name="Channel 7" Type="FP Point"/>
		</Item>
		<Item Name="cFP-RTD-122 @3" Type="FP Device">
			<Item Name="All" Type="FP Point"/>
			<Item Name="Channel 0" Type="FP Point"/>
			<Item Name="Channel 1" Type="FP Point"/>
			<Item Name="Channel 2" Type="FP Point"/>
			<Item Name="Channel 3" Type="FP Point"/>
			<Item Name="Channel 4" Type="FP Point"/>
			<Item Name="Channel 5" Type="FP Point"/>
			<Item Name="Channel 6" Type="FP Point"/>
			<Item Name="Channel 7" Type="FP Point"/>
		</Item>
		<Item Name="cFP-RTD-122 @4" Type="FP Device">
			<Item Name="All" Type="FP Point"/>
			<Item Name="Channel 0" Type="FP Point"/>
			<Item Name="Channel 1" Type="FP Point"/>
			<Item Name="Channel 2" Type="FP Point"/>
			<Item Name="Channel 3" Type="FP Point"/>
			<Item Name="Channel 4" Type="FP Point"/>
			<Item Name="Channel 5" Type="FP Point"/>
			<Item Name="Channel 6" Type="FP Point"/>
			<Item Name="Channel 7" Type="FP Point"/>
		</Item>
		<Item Name="cFP-RTD-122 @5" Type="FP Device">
			<Item Name="All" Type="FP Point"/>
			<Item Name="Channel 0" Type="FP Point"/>
			<Item Name="Channel 1" Type="FP Point"/>
			<Item Name="Channel 2" Type="FP Point"/>
			<Item Name="Channel 3" Type="FP Point"/>
			<Item Name="Channel 4" Type="FP Point"/>
			<Item Name="Channel 5" Type="FP Point"/>
			<Item Name="Channel 6" Type="FP Point"/>
			<Item Name="Channel 7" Type="FP Point"/>
		</Item>
		<Item Name="cFP-RTD-122 @6" Type="FP Device">
			<Item Name="All" Type="FP Point"/>
			<Item Name="Channel 0" Type="FP Point"/>
			<Item Name="Channel 1" Type="FP Point"/>
			<Item Name="Channel 2" Type="FP Point"/>
			<Item Name="Channel 3" Type="FP Point"/>
			<Item Name="Channel 4" Type="FP Point"/>
			<Item Name="Channel 5" Type="FP Point"/>
			<Item Name="Channel 6" Type="FP Point"/>
			<Item Name="Channel 7" Type="FP Point"/>
		</Item>
		<Item Name="cFP-RTD-124 @7" Type="FP Device">
			<Item Name="All" Type="FP Point"/>
			<Item Name="Channel 0" Type="FP Point"/>
			<Item Name="Channel 1" Type="FP Point"/>
			<Item Name="Channel 2" Type="FP Point"/>
			<Item Name="Channel 3" Type="FP Point"/>
			<Item Name="Channel 4" Type="FP Point"/>
			<Item Name="Channel 5" Type="FP Point"/>
			<Item Name="Channel 6" Type="FP Point"/>
			<Item Name="Channel 7" Type="FP Point"/>
		</Item>
		<Item Name="server.vi" Type="VI" URL="../server.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="ErrorConvert.vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/SubVIs/ErrorConvert.vi"/>
				<Item Name="FP Read (Boolean -IO).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Polymorphic Read.llb/FP Read (Boolean -IO).vi"/>
				<Item Name="FP Read (Boolean Array -IO).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Polymorphic Read.llb/FP Read (Boolean Array -IO).vi"/>
				<Item Name="FP Read (Boolean Array).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Polymorphic Read.llb/FP Read (Boolean Array).vi"/>
				<Item Name="FP Read (Boolean).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Polymorphic Read.llb/FP Read (Boolean).vi"/>
				<Item Name="FP Read (Float -IO).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Polymorphic Read.llb/FP Read (Float -IO).vi"/>
				<Item Name="FP Read (Float Array -IO).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Polymorphic Read.llb/FP Read (Float Array -IO).vi"/>
				<Item Name="FP Read (Float Array).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Polymorphic Read.llb/FP Read (Float Array).vi"/>
				<Item Name="FP Read (Float).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Polymorphic Read.llb/FP Read (Float).vi"/>
				<Item Name="FP Read (Polymorphic).vi" Type="VI" URL="/&lt;vilib&gt;/FieldPoint/Polymorphic Read.llb/FP Read (Polymorphic).vi"/>
				<Item Name="FPLVMgr.dll" Type="Document" URL="/&lt;vilib&gt;/FieldPoint/SubVIs/FPLVMgr.dll"/>
				<Item Name="Modbus Slave.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Modbus/slave/Modbus Slave.lvclass"/>
				<Item Name="SubVIs.lvlib" Type="Library" URL="/&lt;vilib&gt;/Modbus/subvis/SubVIs.lvlib"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="My Real-Time Application" Type="{69A947D5-514E-4E75-818E-69657C0547D8}">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{89FD324A-312A-4CB9-AD0A-E35C9615DB70}</Property>
				<Property Name="App_INI_GUID" Type="Str">{E1F2D32A-CA3F-450E-BDE9-AADB86EA1132}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{54D4E59E-704E-4602-A516-70A7956B2B79}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">My Real-Time Application</Property>
				<Property Name="Bld_compilerOptLevel" Type="Int">0</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/NI_AB_TARGETNAME/My Real-Time Application</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{16948E89-AA16-4EE6-9E6A-585FBD4FBC7F}</Property>
				<Property Name="Bld_targetDestDir" Type="Path">/c/ni-rt/startup</Property>
				<Property Name="Bld_version.build" Type="Int">4</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">startup.rtexe</Property>
				<Property Name="Destination[0].path" Type="Path">/c/ni-rt/startup/startup.rtexe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/c/ni-rt/startup/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{084259EA-6ABA-499B-816F-C61490EC0D1D}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/FP Target/server.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref"></Property>
				<Property Name="Source[2].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Library</Property>
				<Property Name="SourceCount" Type="Int">3</Property>
				<Property Name="TgtF_fileDescription" Type="Str">My Real-Time Application</Property>
				<Property Name="TgtF_internalName" Type="Str">My Real-Time Application</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2020 </Property>
				<Property Name="TgtF_productName" Type="Str">My Real-Time Application</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{B21AFE34-2A06-4E90-9B8D-AD65B2A4C8C8}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">startup.rtexe</Property>
			</Item>
		</Item>
	</Item>
</Project>
