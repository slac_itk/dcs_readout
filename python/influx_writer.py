#!/usr/bin/python3.6
import time
from pymodbus.constants import Endian
from pymodbus.client.sync import ModbusTcpClient
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder
import time
from datetime import datetime
from influxdb import InfluxDBClient
import _thread
host='192.168.1.1'
username='atlas'
password='AtlasItkDataBase'
database='lab'
port=8086
def process_loop_1(client):
    while True:
        result = client.read_input_registers(0,48)
        decoder = BinaryPayloadDecoder.fromRegisters(result.registers,
                                                     byteorder=Endian.Big,
                                                     wordorder=Endian.Big)
        channels=[  0] *24
        values=dict()
        for i in range(24):
            channels[i]=decoder.decode_32bit_float()
            channel_name= "rtd_%02u" % i
            values[channel_name]= channels[i]   
            json_body=[ {
                "measurement": "RTDs",
                "tags" : {"location": "lab"},
                "time" : datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'),
                "fields": values
            }]

            db_client = InfluxDBClient(host, port, username, password,database)
            db_client.write_points(json_body)
        time.sleep(5)
        
def process_loop_2(client):
    while True:
        nchannel=(8+16)
        result = client.read_input_registers(0,nchannel*2)
        decoder = BinaryPayloadDecoder.fromRegisters(result.registers,
                                                     byteorder=Endian.Big,
                                                     wordorder=Endian.Big)
        channels=[  0] * nchannel
        values=dict()
        for i in range(8):
            channels[i]=decoder.decode_32bit_float()
            channel_name= "t_%02u" % i
            values[channel_name]= channels[i]   
            json_body=[ {
                "measurement": "CO2",
                "tags" : {"location": "lab"},
                "time" : datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'),
                "fields": values
            }]
            db_client = InfluxDBClient(host, port, username, password,database)
            db_client.write_points(json_body)
        for i in range(16):
            channels[i+8]=decoder.decode_32bit_float()
            channel_name= "p_%02u" % i
            values[channel_name]= (channels[i+8]-0.004 ) / 0.016*100
            json_body=[ {
                "measurement": "CO2",
                "tags" : {"location": "lab"},
                "time" : datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'),
                "fields": values
            }]
            db_client = InfluxDBClient(host, port, username, password,database)
            db_client.write_points(json_body)
        time.sleep(5)

def loop_1():
    while True:
        client = ModbusTcpClient('192.168.1.3',port=8000)
        try:
            process_loop_1(client)
        except Exception as e:
            print(e)
            client.close()
        time.sleep(1)
def loop_2():
    while True:
        client = ModbusTcpClient('192.168.1.3',port=8001)
        try:
            process_loop_2(client)
        except Exception as e:
            print(e)
            client.close()
        time.sleep(1)
_thread.start_new_thread( loop_1 , ())
_thread.start_new_thread( loop_2 , ())

while True:
    time.sleep(1)
