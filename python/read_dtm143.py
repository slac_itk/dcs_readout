#!/usr/bin/env python3
import serial
import sys
import re
import time
from datetime import datetime
from influxdb import InfluxDBClient
import _thread
host='192.168.1.1'
username='atlas'
password='AtlasItkDataBase'
database='lab'
port=8086

ser = serial.Serial('/dev/ttyUSB1', 19200, timeout=1)
ser.write(b'form 3.2 Tdf "," 3.2 Tdfa "," 6.0 H2O "," STAT ":"#r#n \r\n')
ser.write(b'r\r\n')


while(True):
    try:
        line=str(ser.readline())
#        print(line, flush=True)
        line=line.replace(' ','')
        line=line.rstrip('\n').rstrip('\r')
        line=line.split(',')

        t1=float(line[0].replace("b'",''))
        t2=float(line[1])
        ppm=float(line[2])
        stat=line[3].split(':')[0]        
        status=0
        if(stat=='h'): status=1
        if(stat=='H'): status=2
        if(stat=='A'): status=3

        json_body=[ {
                "measurement": "dtm143",
                "tags" : {"location": "lab"},
                "time" : datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'),
                "fields": { 'Tdf' : t1, "Tdfatm" : t2, 'H2O' : ppm, 'status' : status} }]
        db_client = InfluxDBClient(host, port, username, password,database)
        db_client.write_points(json_body)
    except Exception as e: print(e)
    time.sleep(1)

