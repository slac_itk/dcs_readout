#!/usr/bin/python3.6
from pymodbus.constants import Endian
from pymodbus.client.sync import ModbusTcpClient
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder

client = ModbusTcpClient('192.168.1.3',port=8000)
channels=(8*6)
print(channels)
result = client.read_input_registers(0,channels*2)
decoder = BinaryPayloadDecoder.fromRegisters(result.registers,
                                                 byteorder=Endian.Big ,
                                                 wordorder=Endian.Big)
#print(result.registers)
for i in range(channels):
    print(decoder.decode_32bit_float())
client.close()


client = ModbusTcpClient('192.168.1.3',port=8001)
channels=(8+16)
print(channels)
result = client.read_input_registers(0,channels*2)
decoder = BinaryPayloadDecoder.fromRegisters(result.registers,
                                                 byteorder=Endian.Big ,
                                                 wordorder=Endian.Big)
#print(result.registers)                                                                                                                                                        
for i in range(channels):
    print(decoder.decode_32bit_float())
client.close()
